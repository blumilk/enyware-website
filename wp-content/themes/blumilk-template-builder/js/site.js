// @codekit-prepend "html5shiv.min.js";

jQuery(document).ready(function($) {
    // Remove empty p tags
    $("p:empty").remove();
	
    // Responsive navigation
    function responsiveMenu() {
        var windowWidth = $(window).innerWidth();
        if(windowWidth < 1000) {
            $("#nav-menu-desktop").hide();
            $("#nav-menu-mobile").show();
        } else {
            $("#nav-menu-mobile").hide();
            $("#nav-menu-desktop").show();
            
            if ($("#nav-menu-mobile-content").hasClass("expanded")) {
                $(".nav-menu-mobile-button").trigger('click');
            }
        }
                
    }

    responsiveMenu();
        
    // mobile menu button clicked
    function menuButtonClicked(x) {
        x.classList.toggle("change");
        
        if ($("#nav-menu-mobile-content").hasClass("expanded")) {
            $("#nav-menu-mobile-content.expanded").removeClass("expanded").slideUp();
            $("html").removeClass("body-overflow");
        } else {
            $("#nav-menu-mobile-content").addClass("expanded").slideDown();
            $("html").addClass("body-overflow");
        }
    }
    
    $(window).scroll(function() {
        console.log($(window).scrollTop());
    });
    
    
        
    $(".nav-menu-mobile-button").click(function() {
        menuButtonClicked(this);
    });

    $(window).resize(function() {
        responsiveMenu();
    }); 

});