<?php get_header(); ?>
<?php if(have_posts()) { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<?php if(is_day()) { ?>
					<h2>Archive: <?php echo get_the_date('D M Y'); ?></h2>							
				<?php } elseif(is_month()) { ?>
					<h2>Archive: <?php echo get_the_date('M Y'); ?></h2>	
				<?php } elseif(is_year()) { ?>
					<h2>Archive: <?php echo get_the_date('Y'); ?></h2>								
				<?php } else { ?>
					<h2>Archive</h2>	
				<?php } ?>
				<?php while(have_posts()) {
					the_post(); ?>
					<article>
						<h2>
							<a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
								<?php the_title(); ?>
							</a>
						</h2>
						<?php the_content(); ?>
					</article>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<h1>No posts to display</h1>
			</div>
		</div>
	</div>
<?php } ?>
<?php get_footer(); ?>