<?php
	// Remove the admin bar for logged in users.
	add_filter('show_admin_bar', '__return_false');
	
	// Adding CSS styles
	add_action('wp_enqueue_scripts', 'modest_styles');
	function modest_styles() {
		wp_register_style('screen', get_stylesheet_directory_uri() . '/css/style.css', '', '', 'all');
		wp_enqueue_style('screen');
	}
	
	// Adding JavaScript scripts
	add_action('wp_enqueue_scripts', 'modest_scripts');
	function modest_scripts() {
		wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/min/site-min.js', array('jquery', 'jquery-ui-core'));
	}
	
	// Register the navigation menu.
	add_action('init', 'modest_menu');
	function modest_menu() {
		register_nav_menu('modest-menu',__('Modest Menu'));
	}
        
        add_action('wp_enqueue_scripts', 'animate_styles');
        function animate_styles() {
                wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/css/animate.css' );
                wp_enqueue_style('animate');
        }

        
	// Show Featured Image on posts.
	add_theme_support('post-thumbnails');
	
	// Advanced Custom Fields Option Page
	if(function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title' => 'Contact Details',
			'menu_title' => 'Contact Details',
			'menu-slug' => 'contact-details',
			'icon_url' => 'dashicons-phone'
		));
	}
	
	// Excerpt Length
	add_filter('excerpt_length', 'custom_excerpt_length', 999);
	function custom_excerpt_length($length) {
		return 18;
	}
	
	// Adjust the length of the WordPress content you would like to show
	// the_content_length(360, "...");
	function the_content_length($length, $trail = false) {
		$theContent = apply_filters("the_content", get_the_content());
		$theContent = str_replace("]]>", "]]&gt;", $theContent);
		
		if(strlen($theContent) < $length) {
			echo $theContent;
		} else {
			// Checks to see if there's a value for $trail
			if(!empty($trail)) {
				echo trim(substr($theContent, 0, $length)) . $trail;
			} else {
				echo trim(substr($theContent, 0, $length)) . "...";
			}
		}
	}
	
	// Removes squared brackets from the reset password link
	// Very useful when you require emails to be formatted to HTML
	function removeBracketsHTML($message, $key, $user_login, $user_data) {
		return preg_replace('#<(https?:\/\/.+?)>#i', '$1', $message);
	}
	add_filter('retrieve_password_message', 'removeBracketsHTML', 100, 4);
	
	// Loads the login.css stylesheet for when a client is logging in
	function loginStylesJGD() {
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login.css" />';
	}
	add_action('login_head', 'loginStylesJGD');
	
	function logoURLJGD() {
		return 'http://www.blumilk.com';
	}
	add_filter('login_headerurl', 'logoURLJGD');
	
	// Loads the login.css stylesheet for when a client is logging in
	function adminStylesJGD() {
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login.css" />';
	}
	add_action('admin_head', 'adminStylesJGD');
        
        
        
?>