<?php
/*
    Template Name: Page Template
*/

get_header();

$page_layout = get_field('layout');

$html_output = '';

$element_count = 0;

function processColumnWidth($column_width) {
    global $html_output;
    
    foreach ($column_width as $width_key => $width_value) {
        if ($width_value !== '') {
            $html_output .= ' ' . $width_key . '-' . $width_value;
        }
    }
}

function processSelect($column_element) {
    switch ($column_element['acf_fc_layout']) {
        case 'image_element':
            processImageElement($column_element);
        break;
    
        case 'header_element':
            processHeaderElement($column_element);
        break;
    
        case 'spacer_element':
            processSpacerElement($column_element);
        break;
    
        case 'text_element':
            processTextElement($column_element);
        break;
    
        case 'slider_element':
            processSliderElement($column_element);
        break;
    
        case 'html_element':
            processHtmlElement($column_element);
        break;

    }
}

function processImageElement($image_element) {
    global $html_output;
    
    $html_output .= '<div class="image-element">';
    $html_output .= '<img src="' . $image_element['image_url'] . '">';
    $html_output .= '</div>';
}

function processHeaderElement($header_element) {
    global $html_output;
    
    $html_output .= '<div class="header-element">';
    $html_output .= '<strong><p style="text-align:' . $header_element['alignment'] . ';">' . $header_element['text'] . '</p></strong>';
    $html_output .= '</div>';
}

function processSpacerElement($spacer_element) {
    global $html_output;
    
    $html_output .= '<div class="header-element" style="height:' . $spacer_element['height'] . 'px;">';
    $html_output .= '</div>';
}

function processTextElement($text_element) {
    global $html_output;
    
    $html_output .= '<div class="text-element">';
    $html_output .= '<p style="text-align:' . $text_element['alignment'] . ';">' . $text_element['text'] . '</p>';
    $html_output .= '</div>';
}

function processSliderElement($slider_element) {
    global $html_output;
    
    $html_output .= '<div class="slider-element">';
    $select_first = '';

    foreach($slider_element['slides'] as $slide_key => $slide) {
        $html_output .= '<div class="slide owl-item' . $select_first . '">';
            $html_output .= '<img src="' . $slide['image']['url'] . '">';
            $html_output .= '<div class="slide-content">';
                $html_output .= '<div class="slide-header">' . $slide['header'] . '</div><br>';
                $html_output .= '<div class="slide-text">' . $slide['text'] . '</div>';
            $html_output .= '</div>';
        $html_output .= '</div>';
        
        $select_first = '';
    }

    $html_output .= '</div>';
}

function processHtmlElement($html_element) {
    global $html_output;
    
    $html_output .= '<div class="html-element">';
    $html_output .= $html_element['html'];
    $html_output .= '</div>';
}


$html_output .= '<div class="main_content">';

// get grid layout
if ($page_layout) {

    foreach($page_layout as $layout) {
        
        $html_output .= '<div class="layout-section">';
       
        switch ($layout['acf_fc_layout']) {
            
            case 'grid':
                
                $html_output .= '<div class="grid-section">';
                
                foreach ($layout['rows'] as $row) {
                    
                    $column_count = count($row['columns']);
                    
                    $html_output .= '<div class="row-section">';

                    foreach($row['columns'] as $column) {

                        $html_output .= '<div class="column';
                        processColumnWidth($column["column_width"]);
                        $html_output .='">';
                        
                        foreach($column['column_elements'] as $column_element) {
                            processSelect($column_element);
                        }
                        
                        $html_output .= '</div>';
                    }

                    $html_output .= '</div>';
                }
                
                $html_output .= '</div>';
                 
            break;
                
        }
        $html_output .= '</div>';

    }
    
    $html_output .= '</div>';
       
}

echo $html_output;

get_footer();