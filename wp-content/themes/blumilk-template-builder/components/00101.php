<section class="comp comp-00101 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<div class="dt-12">
				<ul class="slider">
					<?php while(have_rows('slider')) {
						the_row();
						$image = get_sub_field('image'); ?>
						<li style="background-image: url('<?php echo $image['url']; ?>');"></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<script>
	jQuery('.slider').owlCarousel({
		loop: true,
		margin: 0,
		items: 1,
		autoplay: true,
		autoplayTimeout: <?php the_sub_field('slider_speed'); ?>,
		slideSpeed: 1500,
		navigation: true,
		lazyLoad: true,
	});
</script>