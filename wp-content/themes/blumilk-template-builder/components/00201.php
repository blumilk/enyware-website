<section class="comp comp-00201">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<?php while(have_rows('gallery')) {
				the_row(); ?>
				<div class="tl-3 tp-6">
					<?php $image = get_sub_field('image'); ?>
					<img src="<?php echo $image['url']; ?>">
				</div>
			<?php } ?>
		</div>
	</div>
</section>