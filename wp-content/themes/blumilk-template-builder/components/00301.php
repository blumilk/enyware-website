<section class="comp comp-00301 <?php the_sub_field('background_colour'); ?> <?php the_sub_field('text_colour'); ?> <?php the_sub_field('text_alignment'); ?>">
	<div class="wrapper">
		<div class="row">
			<div class="tl-8 indent-tl-2">
				<strong><?php the_sub_field('bold_text'); ?></strong>
				<hr class="<?php the_sub_field('text_colour'); ?> size-m width-s">
				<?php the_sub_field('content'); ?>
			</div>
		</div>
	</div>
</section>