<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'enyware-website');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Gf3Ub=58X+|^[K~IAS 4^<*.bs%Mjf@4wBwBCRuQ7@fdI?mFeB?4^S/8>6,i4,DZ');
define('SECURE_AUTH_KEY',  'QqYH0H[ U<}q!-LAtWTIZ1h&`[eG5@SE~tdRSWO*kj5.>ZG<^8dvw3ML4$2a:|8n');
define('LOGGED_IN_KEY',    'y>AjBepMc!X%LA2z#$0+0-voK7`6r@1s%(0y8~dFIu-e+k^S2u4|TlU4we|21q+9');
define('NONCE_KEY',        'X r3|GMBfg[WNi8IL,>:;pOJW-MH;tW,{FpZ}XTKVO1>k7>KO_X0CZ^rCMb=7eCh');
define('AUTH_SALT',        'Na{c~7ERn~I?9LSB;+-bZW-4mP2jW|*U6SY0i|++NM57fX-fp(F>,WD+9$})^h+v');
define('SECURE_AUTH_SALT', 'NP4{)%jd5=9e2~+{*BuO-3>5>AL:?eYL+v!M.m6`C0T7&iD.++l>K6E*CQ:vId!8');
define('LOGGED_IN_SALT',   '(tH3dDWku!ay`0^-9u#1NNB|epxyaKm@(wc|E0/G#bHx,1<%|`57sl7,Vk-:F;]|');
define('NONCE_SALT',       '-iX$E w8AO!+xdi+rk?(U[!^s$O;gyq<+on1Q(hS~hhnk2(p,VX`()>O%g8]-+fx');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ew_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
